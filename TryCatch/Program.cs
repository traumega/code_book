﻿using System;

namespace TryCatch
{
    class Program
    {
        static void Main(string[] args)
        {
           System.Console.WriteLine("what is your age");
           string input = System.Console.ReadLine();

           try
           {
                int age = int.Parse(input);
                System.Console.WriteLine($"your age is {age}");
           }
           catch(OverflowException)
           {
               System.Console.WriteLine("the age you entered is to large.");
           }
           catch(FormatException)
           {
               System.Console.WriteLine("the age you entered is not a valid number format.");
           }
           catch(Exception ex)
           {
               System.Console.WriteLine($"{ex.GetType()} says {ex.Message}");
           }
               System.Console.WriteLine("after parse");

        }
    }
}
