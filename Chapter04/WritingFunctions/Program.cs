﻿using System;
using static System.Console;

namespace WritingFunctions
{
     public enum Days {Monday, Tuesday, Wednsday, Thursday, Friday, Saturday, Sunday};
    class Program
    {
        static void TimesTable(byte number)
        {
            System.Console.WriteLine($"this is the {number} times table:");
            for(int row =1; row <= 12; row++)
            {
                System.Console.WriteLine($"{row} x {number} = {row * number}");
                System.Console.WriteLine();

            }
        }

        static void RunTimesTable()
        {
            bool isNumber;
            do
            {
                Write("enter a number between 0 and 255");
                //using tryparse to read in put and try to pass the value to the bool
                isNumber = byte.TryParse(ReadLine(), out byte number);
                //if is number is true
                if (isNumber)
                {
                    TimesTable(number);
                }
                //if is number is false
                else
                {
                    System.Console.WriteLine("you did not enter a valid number!");
                }
            
            }
            while(isNumber);
        }

        static decimal CalculateTax(decimal amount, string twoLetterRegionCode)
        {
            decimal rate = 0.0M;
            switch (twoLetterRegionCode)
            {
                case "CH":
                    rate = 0.08M;
                    break;

                case "DK":
                case "NO":
                    rate = 0.25M;
                    break;
                case "GB":
                case "FR":
                    rate = 0.2M;
                    break;
                case "HU":
                    rate = 0.27M;
                    break;
                case "OR":
                case "AK":
                case "MT":
                    rate = 0.0M;
                    break;
                case "ND":
                case "WI":
                case "ME":
                case "VA":
                    rate = 0.05M;
                    break;
                case "CA":
                    rate = 0.0825M;
                    break;
                default:
                    rate = 0.06M;
                    break;
            }
            return amount * rate;
        }

        static void RunCalculateTax()
        {
            Write("Enter an amount: "); 
            string amountInText = ReadLine(); 
            Write("Enter a two-letter region code: "); 
            string region = ReadLine(); 
            if (decimal.TryParse( amountInText, out decimal amount)) 
            { 
                decimal taxToPay = CalculateTax( amount, region);
                WriteLine( $" You must pay {taxToPay} in sales tax."); 
            } 
            else 
            { 
                WriteLine("You did not enter a valid amount!"); 
            }

        }

        static string CardinalToOrdinal(int number)
        {
            switch (number)
            {
                case 11:
                case 12:
                case 13:
                    return $"{number}th";
                default:
                    int lastDigit = number % 10;
                    string suffix = lastDigit switch
                    {
                        1 => "st",
                        2 => "nd",
                        3 => "rd",
                        _ => "th"
                    };
                    return $"{number}{suffix}";

            }
        }

        static void runCardinalToOrdinal()
        {
            for(int number = 1; number <= 40; number ++)
            {
                System.Console.WriteLine($"{CardinalToOrdinal(number)}");
            }
            WriteLine();
        }

        static int Factorial(int number)
        {
            if (number < 1)
            {
                return 0;
            }
            else if (number < 1)
            {
                return 1;
            }
            else
            {
                return number * Factorial(number -1);
            }
        }

        static void RunFactorial()
        {
            for(int i =1; i < 15; i++)
            {
                System.Console.WriteLine($"{i}! = {Factorial(i):NO}");
            }
        }
        static void Main(string[] args)
        {
            //RunTimesTable();
            // RunCalculateTax();
            runCardinalToOrdinal();
            // int x = 0;
            // do
            // {
            //     System.Console.WriteLine("enter a number");
            //     string result = ReadLine();            
            //     int number = Int32.Parse(result) % 10;
            //     System.Console.WriteLine(number);
            //     x++;
            // }
            // while(x < 10);
           
             
            
        }
        
    }
        
}

