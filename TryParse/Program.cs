﻿using System;
using System.IO;
using static System.Console;

namespace TryParse
{
    class Program
    {
        static void Main(string[] args)
        {
            System.Console.WriteLine("how many eggs are there now ff gg rere?");
            int count;
            string input = ReadLine();
            string parser = (int.TryParse(input,out count)) ? $"there are {count} eggs." : "I could not parse the input";
            System.Console.WriteLine(parser);
        }
    }
}
